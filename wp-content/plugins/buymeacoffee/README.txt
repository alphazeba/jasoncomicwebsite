=== Buy Me A Coffee - Free and Beautiful Donate Button ===
Contributors: buymeacoffee
Donate link: https://www.buymeacoffee.com/us
Tags: Buy Me A Coffee, Donate Button, PayPal Button, Wordpress Donation Plugin, Donate Widget, bmc, Apple Pay
Requires at least: 3.0.1
Tested up to: 4.9.1
Requires PHP: 5.2
Stable tag: 1.1.9
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl.html

Receive donations in a fast and friendly way. Accept Paypal, Credit Card (Stripe), Apple Pay and Android Pay. Cryptocurrency (Bitcoin, Litecoin & Ethereum) coming soon.

== Description ==
BuyMeACoffee is a free platform used by thousands of creators to accept donations, and it has finally come to Wordpress. Place the beautiful ‘Buy Me A Coffee’ widget on your blog today! 

Widely considered the fastest (one-click donation) and friendliest way to accept donations, BuyMeACoffee will allow your audience to support you in less a minute (5x faster compared to using a PayPal button). It takes less than 3 minutes to set up your ‘Buy Me A Coffee’ page and button to start receiving contributions. It is as simple as that. 

BuyMeACoffee is open to creators all over the world, and of all sizes and types (art, blog, open source, comic - you name it!). 

Payouts are instantly processed through Paypal and Stripe (direct bank transfer). 


== Installation ==
Requirements

1. A BuyMeACoffee.com account (create account)
2. The latest version of WordPress or at least 4.5 is recommended.
3. Your server must support Curl for HTTP requests to be processed.
4. XML-RPC enabled and publicly accessible

Installation
1. Log in to your site’s Dashboard (e.g. http://www.yourawesomedomain.com/wp-admin)
2. In the left panel, click “Plugins”, then click “Add New”.
3. Search for “BuyMeACoffee” — the latest version will appear at the top of the list of results.
4. Click the “Install Now” button.
5. Wait for the installation to finish, then click the “Activate” button.
6. Click the “Connect to BuyMeACoffee.com” button. Log in with your BuyMeACoffee.com account or create an account.
7. Wait for the connection process to finish.
8. You will be returned to the Plugin Dashboard


== Frequently Asked Questions ==
=What is BuyMeACoffee?=
BuyMeACoffee is a platform for creators to set up a support page so that their supporters can buy them coffees in the form of $3/$4/$5 donations.

=How do I get paid?=

We offer Paypal and Stripe as payout options. Creators from Stripe-supported countries (US, UK, Canada and 22 others) are eligible to receive instant payouts without any minimum threshold. Payout via Paypal is available for creators all over the world and is paid when the support amount reaches a minimum threshold of $10.

=Is there an actual coffee included?=

Nope, sadly. Coffee is just a metaphor creators use to get support. Each coffee represents a $3/$4/$5 donation, and it is up to you how to spend it.

=Is there a fee to using BuyMeACoffee?=

BuyMeACoffee does not charge a monthly fee, and only make money when the creator is making money. BMC takes 5% platform fee every time a supporter buys you a coffee. So when a creator receives $100 from supporters, BMC will earn a $5 platform fee.

We are partnered with Paypal and Stripe to process payments, and they charge some cents per successful payment.

=Is there a minimum requirement to set up a page?=

None, Ever. BuyMeACoffee is open to creators of all sizes, and there is no minimum requirement to set up a page and start receiving support.

=How do I contact BuyMeACoffee=

We are always an email away in case you need something.





== Changelog ==


= 1.1.9 =

* Introducing color picker's for more customization.

* Other Improvements.


= 1.1.8 =

* Major Bug Fixes

* UI enhancements


= 1.1.7 =

* Bug Fixes

= 1.1.6 =

* Bug Fixes

= 1.1.3 =

* Bug Fixes

= 1.1.2 =

* Bug Fixes and UI improvements.

= 1.1.1 =

* Optimized session handling.
* More customization options for widget.

= 1.1.0 =

* Major bug Fixes.
* Security patches applied.
* UI improvements.
* Fixed backwards compatibility issues.
* Added support for various shared hosting configurations.
* User information information handling on disconnection.

= 1.0.9 =

* Bug Fixes.


= 1.0.8 =

* Bug Fixes and UI improvements.

= 1.0.7 =

* Bug Fixes.

= 1.0.6 =

* Bug Fixes.

= 1.0.5 =

* Bug Fixes.

= 1.0.4 =

* Bug Fixes.

= 1.0.3 =

* Bug Fixes.

= 1.0.2 =

* Bug Fixes.

= 1.0.1 =

* Bug Fixes.

= 1.0.0 =

* Initial release.
