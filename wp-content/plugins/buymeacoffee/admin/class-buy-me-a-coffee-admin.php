<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.buymeacoffee.com
 * @since      1.0.0
 *
 * @package    Buy_Me_A_Coffee
 * @subpackage Buy_Me_A_Coffee/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Buy_Me_A_Coffee
 * @subpackage Buy_Me_A_Coffee/admin
 * @author     Buymeacoffee <hello@buymeacoffee.com>
 */
class Buy_Me_A_Coffee_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;



    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;
        $this->view = new Buy_Me_A_Coffee_Admin_View();



    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Buy_Me_A_Coffee_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Buy_Me_A_Coffee_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/buy-me-a-coffee-admin.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Buy_Me_A_Coffee_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Buy_Me_A_Coffee_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/buy-me-a-coffee-admin.js', array(
            'jquery'
        ), $this->version, false);

    }

    public function bmc_menu()
    {
        add_menu_page(__('BuyMeACoffee', 'bmc-menu'), __('BuyMeACoffee', 'bmc-menu'), 'manage_options', 'buy-me-a-coffee', array(
            $this->view,
            'bmc_show_data'),'data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyMCAyMCI+PHRpdGxlPmRvd25sb2FkPC90aXRsZT48cGF0aCBkPSJNMTUuMTQsMi4zOSwxNCwwSDZMNC44NCwyLjM5SDMuMjh2Mi4xaC40OEw2LjE2LDIwaDcuNjVsMi41LTE1LjUxaC40MVYyLjM5Wk0xMi45MywxOUg3TDQuODMsNC42OEgxNS4yNFoiIGZpbGw9IiM4Mjg3OGMiLz48cG9seWdvbiBwb2ludHM9IjMuNTkgOC4yIDE2LjQxIDguMiAxNS4yIDE0Ljk3IDQuODggMTQuOTcgMy41OSA4LjIiIGZpbGw9IiM4Mjg3OGMiLz48L3N2Zz4=');
    }



    public function bmc_activation_redirect()
    {
		update_option('BMC_Widget_disconnect' , 1 );
      exit( wp_redirect( admin_url( 'admin.php?page=buy-me-a-coffee' ) ) );
    }

    function bmc_register_plugin() {
      $widget = new BMC_Widget( );
	  if ( get_option('BMC_Widget_disconnect') != 1 ) {
        register_widget($widget);

	  }
    }

    public function bmc_disconnect()
    {
		unregister_widget( 'BMC_Widget' );
		delete_option('widget_buymeacoffee_widget');
		update_option('BMC_Widget_disconnect' , 1 );

      global $wpdb;

      $table = $wpdb->prefix . 'bmc_plugin';

      $where = array('slug'=>$_POST['slug']);

      $wpdb->delete( $table, $where, $where_format = null );

      die(exit( wp_redirect( admin_url( 'admin.php?page=buy-me-a-coffee' ) ) ));

    }

    public function recieve_post()
    {
      status_header(200);

      global $wpdb;

      $table = $wpdb->prefix . 'bmc_plugin';

      echo $_POST['font_family'];
      $data = array('background_color'=>$_POST['background_color'],
      'text_color' => $_POST['text_color'],
      'widget_text' => $_POST['text'],
        'font_family' => $_POST['font_family']
    );

    $where = array('slug'=>$_POST['slug']);

      $wpdb->update( $table, $data, $where );
	  update_option('BMC_Widget_disconnect' , 0 );
      die(exit( wp_redirect( admin_url( 'admin.php?page=buy-me-a-coffee&status=true' ) ) ));
      // die("Server received ".$_POST['slug']." from your browser.");
     //request handlers should die() when they complete their task
    }

}
